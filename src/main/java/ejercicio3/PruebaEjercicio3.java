package ejercicio3;

/*Ahora, crea una clase clase ejecutable:

Crea un array de Passwords con el tamaño que tu le indiques por teclado.
Crea un bucle que cree un objeto para cada posición del array.
Indica también por teclado la longitud de los Passwords (antes de bucle).
Crea otro array de booleanos donde se almacene si el password del array de Password es o no fuerte (usa el bucle anterior).

Al final, muestra la contraseña y si es o no fuerte (usa el bucle anterior). Usa este simple formato:

contraseña1 valor_booleano1

contraseña2 valor_bololeano2
*/

import java.util.Locale;
import java.util.Scanner;

public class PruebaEjercicio3 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\n");
        sc.useLocale(Locale.US);

        //Crear numeros de pass
        System.out.println("Introduce numero de claves a generar");
        int tamano = sc.nextInt();

        System.out.println("Introduce la longitud de las contraseñas");
        int logitud = sc.nextInt();

        Password listaPasswod[] = new Password[tamano];
        boolean passwordSeguras[] = new boolean[tamano];

        for (int i = 0; i < listaPasswod.length; i++) {
            listaPasswod[i] = new Password(logitud);
            passwordSeguras[i] = listaPasswod[i].esFuerte();
            System.out.println("Contraseña: " + listaPasswod[i].getContraseña() + " - "+ "Seguridad: " + passwordSeguras[i] );
        }
    }
}
