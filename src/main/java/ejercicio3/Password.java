package ejercicio3;

 /*Haz una clase llamada Password que siga las siguientes condiciones:

     Que tenga los atributos longitud y contraseña . Por defecto, la longitud sera de 8.

     Los constructores serán los siguiente:

     Un constructor por defecto.
     Un constructor con la longitud que nosotros le pasemos. Generara una contraseña aleatoria con esa longitud.

     Los métodos que implementa serán:


     generarPassword():  genera la contraseña del objeto con la longitud que tenga.


     Método get para contraseña y longitud.
     Método set para longitud.

 */


import java.util.Random;

public class Password {

    private final static int LOGITUD_DEF = 8;

    private int logitud;
    private String contraseña;

    public Password() {
        this(LOGITUD_DEF);
    }

    public Password(int logitud) {
        this.logitud = logitud;
        this.contraseña = generarPassword();
    }

    public int getLogitud() {
        return logitud;
    }

    public void setLogitud(int logitud) {
        this.logitud = logitud;
    }

    public String getContraseña() {
        return contraseña;
    }


    private String generarPassword(){

        // Genero contraseña apartir de la logintud, genero 3 tipo de caracteres distintos q son (minuscula, mayuscula y numero)
        // segun tabla Asii las minusculas van 97 hasta 123, mayusculas 65 hasta 91 y los numeros de 48 a 58
        // apartir de esto geneno caractares randoms y añado cada caracter a la variable contraseña hasta llegar a tener una logintud de 8 caracteres
        // (Math.ramdom() * (max - min)) + min

        this.contraseña = "";
        for (int i = 0; i < logitud; i++) {

            int tipoCaracterPass =((int)Math.floor((Math.random()*3) + 1));

            if (tipoCaracterPass == 1) {
                char miniscula = (char) ((int)Math.floor((Math.random()* (123-97))+97));
                contraseña += miniscula;
            }else {
                if (tipoCaracterPass == 2){
                    char mayuscula =  (char) ((int) Math.floor(((Math.random()* (91-65))+65)));
                    contraseña += mayuscula;
                }else {
                    char numeros = (char) ((int) Math.floor(Math.random()*(58-48) + 48));
                    contraseña += numeros;
                }
            }
        }

        return contraseña;
    }

    //esFuerte(): devuelve un booleano si es fuerte o no, para que sea fuerte debe tener mas de 2 mayúsculas, mas de 1 minúscula y mas de 5 números.

    public boolean esFuerte(){
        int cuentaMinuscula = 0;
        int cuentaMayuscula = 0;
        int cuentaNumeros = 0;
        boolean esFuerte = false;

        for (int i = 0; i < contraseña.length() ; i++) {
            if (contraseña.charAt(i) >= 97 && contraseña.charAt(i) <= 122){
                cuentaMinuscula ++;
            }else {
                if (contraseña.charAt(i)  >= 65 && contraseña.charAt(i) <= 90) {
                    cuentaMayuscula ++;
                }else {
                    cuentaNumeros ++;
                }
            }

        }
        if (cuentaMayuscula >= 2 && cuentaMinuscula >= 1 && cuentaNumeros >= 5){
            esFuerte = true;
        }

        return esFuerte;
    }



    @Override
    public String toString() {
        return "Password{" +
                "logitud=" + logitud +
                ", contraseña='" + contraseña + '\'' +
                '}';
    }
}
