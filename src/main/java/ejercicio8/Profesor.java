package ejercicio8;

public class Profesor extends Persona {
    private String materia;

    public Profesor() {
        super();
        this.materia = obtenerMateria();
        super.setEdad(super.valoresAleatorios(25,50));
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }


    private String obtenerMateria(){
        return  Persona.MATERIA_DEF[super.valoresAleatorios(0,2)];
    }

    //    El profesor tiene un 20% de no encontrarse disponible (reuniones, baja, etc.)
    @Override
    public void disponibilidad() {

        int disponible = super.valoresAleatorios(0,100);
        if (disponible < 20){
            super.setAsistencia(false);
        }else {
            super.setAsistencia(true);
        }
    }
}
