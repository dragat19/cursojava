package ejercicio8;

public abstract class Persona {

    protected String nombre;
    protected char sexo;
    protected int edad;


    private boolean asistencia;


    protected final static String  MATERIA_DEF[] = {"matematicas","fisiologia","fisica"};


    private final static char SEXO_DEF[] = {'M','F'};
    private final static int EDAD_DEF[] = {15,16,18,30,80};
    private final static String NOMBRE_HOMBRE_DEF[] = {"Albert","Jose","Victor","Angel","Rogelio"};
    private final  static String NOMBRE_MUJER_DEF[] = {"Rosa","Maria","Laura","Patricia","Clara"};



    public Persona() {
        this.sexo =  SEXO_DEF[valoresAleatorios(0,1)] ;

        if(this.edad < 80){
            this.edad = EDAD_DEF[valoresAleatorios(0,4)];
        }

        this.nombre = obtenerNombreSegunSexo();

        disponibilidad();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public char getSexo() {
        return sexo;
    }

    public int getEdad() {
        return edad;
    }

    public boolean isAsistencia() {
        return asistencia;
    }

    public void setAsistencia(boolean asistencia) {
        this.asistencia = asistencia;
    }



    private String obtenerNombreSegunSexo(){
        if (sexo == 'M') {
            nombre = NOMBRE_HOMBRE_DEF[valoresAleatorios(0,4)];
        }else {
            nombre = NOMBRE_MUJER_DEF[valoresAleatorios(0,4)];
        }
        return nombre;
    }


    public int  valoresAleatorios(int valorInicial, int valorFinal){
        return   (int) (Math.floor(Math.random()* (valorInicial - (valorFinal + 1)) ) + (valorFinal + 1) );
    }


    public  abstract void disponibilidad();
}
