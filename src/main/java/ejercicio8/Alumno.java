package ejercicio8;

public class Alumno  extends Persona{

    private int calificacion;

    public Alumno() {
        super();
        this.calificacion = obtenerNota();
    }


    private int obtenerNota(){
        int notaAlumno = 0;
        notaAlumno = super.valoresAleatorios(0,10);
        return notaAlumno;
    }

    public int getCalificacion() {
        return calificacion;
    }

    @Override
    public void disponibilidad() {
        int disponible = super.valoresAleatorios(0,100);
        if (disponible < 50){
            super.setAsistencia(false);
        }else {
            super.setAsistencia(true);
        }
    }


    @Override
    public String toString() {
        return "Alumno{" +
                "calificacion=" + calificacion +
                ", nombre='" + nombre + '\'' +
                ", sexo=" + sexo +
                ", edad=" + edad +
                '}';
    }
}
