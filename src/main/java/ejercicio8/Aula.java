package ejercicio8;

public class Aula {
    private  static final int ALUMNO_MAX_DEF = 20;

    private  int id;
    private String materiaAula;

    private Alumno alumnos[];
    private Profesor profesor;


    public Aula() {
        id = 1;
        this.materiaAula = Persona.MATERIA_DEF[0];
        alumnos = new Alumno[ALUMNO_MAX_DEF];
        crearAlumnos();
        profesor = new Profesor();
        profesor.setMateria(Persona.MATERIA_DEF[0]);
    }


    private void crearAlumnos(){
        for (int i = 0; i < alumnos.length; i++) {
             alumnos[i] = new Alumno();
            System.out.println(alumnos[i]);
        }
    }


    private boolean asistenciaAlumnos(){
        int contarAsitencia = 0;

        for (int i = 0; i < alumnos.length; i++) {
            if (alumnos[i].isAsistencia()){
                contarAsitencia++;
            }
        }

        //Muestro la asistencia total
        System.out.println("Hay "+contarAsitencia+" alumnos disponibles");
        return  contarAsitencia >=  (int)(alumnos.length/2) ;
    }

    public boolean darClase(){
        boolean sePuedarClase = false;
        if(!profesor.isAsistencia()){
            System.out.println("El profesor no esta, no se puede dar clase");
        }else {
            if (!profesor.getMateria().equals(materiaAula)) {
                System.out.println("La materia del aula y la del profesor no es la mismo, no se puede dar clase");
            }else {
                if (!asistenciaAlumnos()){
                    System.out.println("La asistencia de los alumnos no es sufuciente, no se puede dar clase");
                }else {
                    System.out.println("Se puede dar clase");
                    sePuedarClase = true;
                }
            }
        }
        return sePuedarClase;
    }


    public void cuantosAlumnosAprobaron(){
        int aprobadosMujer = 0;
        int aprobadoHombre = 0;

        for (int i = 0; i <alumnos.length ; i++) {

            if (alumnos[i].getCalificacion() >= 5) {
                if (alumnos[i].getSexo() == 'M') {
                    aprobadoHombre++;
                }else {
                    aprobadosMujer++;
                }
            }

        }
        System.out.println(" Alumnos aprobados: mujer " +aprobadosMujer+ " - Hombre " +aprobadoHombre);


    }

}
