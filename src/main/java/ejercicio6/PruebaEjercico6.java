package ejercicio6;

public class PruebaEjercico6 {
    public static void main(String[] args) {
        Libro libro1 = new Libro("ATRE2355","Codigo limpio","Albert Sanchez",100);
        Libro libro2 = new Libro("AQRT2247","Android tutorial","Rosa Zambrano",500);

        System.out.println(libro1);
        System.out.println(libro2);

        libro1.setNumPaginas(600);

        if ( libro1.getNumPaginas() > libro2.getNumPaginas() ){
            System.out.println("Libro1 tiene mas paginas");
        }else {
            System.out.println("Libro2 tiene mas paginas");
        }

    }
}
