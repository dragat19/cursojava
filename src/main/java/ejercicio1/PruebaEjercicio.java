package ejercicio1;

public class PruebaEjercicio {

    public static void main(String[] args) {
        Cuenta c = new Cuenta("Albert");
        Cuenta c2 = new Cuenta("Rosa",6000);

        c.ingresar(10000);
        c2.ingresar(1000);

        c.retirar(700);
        c2.retirar(100);

        System.out.println(c);
        System.out.println(c2);

    }
}
