package ejercicio4;

public class Lavadora extends Electrodomestico implements Precio {

    private final static double CARGA_DEF = 5;

    private double carga;

    public Lavadora() {
        this(PRECIO_DEF,COLOR_DEF,CONSUMO_DEF,PESO_DEF,CARGA_DEF);
    }

    public Lavadora(double precioBase, double peso) {
        this(precioBase,COLOR_DEF,CONSUMO_DEF,peso,CARGA_DEF);
    }

    public Lavadora(double precioBase, String color, char consumoEnergetico, double peso, double carga) {
        super(precioBase, color, consumoEnergetico, peso);
        this.carga = carga;
    }

    public double getCarga() {
        return carga;
    }

    @Override
    public double precioFinal() {
        double precio = super.precioFinal();
        if (carga > 30) {
            precio =+ 50;
        }

        return precio;
    }
}
