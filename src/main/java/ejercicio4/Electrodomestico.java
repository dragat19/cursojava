package ejercicio4;


public  class Electrodomestico {


    protected final static String COLOR_DEF = "blanco";
    protected final static char CONSUMO_DEF = 'F';
    protected final static double PRECIO_DEF = 100;
    protected final static double PESO_DEF = 5;


    protected double precioBase;
    protected String color;
    protected char consumoEnergetico;
    protected double peso;


    public Electrodomestico() {
        this(PRECIO_DEF,COLOR_DEF,CONSUMO_DEF,PESO_DEF);
    }

    public Electrodomestico(double precioBase, double peso) {
        this(precioBase,COLOR_DEF,CONSUMO_DEF,peso);
    }

    public Electrodomestico(double precioBase, String color, char consumoEnergetico, double peso) {
        this.precioBase = precioBase;
        this.color = color;
        comprobarColor(this.color);
        this.consumoEnergetico = consumoEnergetico;
        comprobarConsumoEnergetico(this.consumoEnergetico);
        this.peso = peso;
    }

    /*comprobarConsumoEnergetico(char letra): comprueba que la letra es correcta, sino es correcta usara la letra por defecto.
    Se invocara al crear el objeto y no sera visible.*/

    private void comprobarConsumoEnergetico(char letra){
        if (consumoEnergetico >= 65 &&  consumoEnergetico <= 70){
            System.out.println("La letra del comsumo energetico es correcto");
            consumoEnergetico = letra;
        }else {
            consumoEnergetico = CONSUMO_DEF;
            System.out.println("La letra del comsumo energetico no correcto asigno: "+ CONSUMO_DEF);
        }
    }

    private void comprobarColor(String color){
        String[] colores = {"blanco","negro","rojo","azul", "gris"};
        boolean isCorrecto = false;
        for (int i = 0; i <colores.length ; i++) {
            if (color.equals(colores[i])){
                isCorrecto = true;
            }
        }

        if(!isCorrecto){
            System.out.println("El color no es el correcto");
            this.color = COLOR_DEF;
        }else {
            this.color = color;
        }

    }



    public double precioSegunConsumo(){
        double precio = 0;
        switch (consumoEnergetico){
            case 'A':
                precio += 100;
                break;
            case 'B':
                precio += 80;
                break;
            case 'C':
                precio += 60;
                break;
            case 'D':
                precio += 50;
                break;
            case 'E':
                precio += 30;
                break;
            case 'F':
                precio += 10;
                break;
        }
        return precio;
    }

    public double precioSegunPeso(){
        double precio = 0;

        if (this.peso >= 0 && this.peso <= 19){
            precio += 10;
        }else {
            if (this.peso >= 20 && this.peso <= 49) {
                precio += 50;
            }else {
                if (this.peso >= 50 && this.peso <= 79){
                    precio += 80;
                }else {
                    precio += 100;
                }
            }
        }

        return precio;
    }

    public double precioFinal(){
        return  precioBase + precioSegunConsumo() + precioSegunPeso();
    }

    public double getPrecioBase() {
        return precioBase;
    }

    public String getColor() {
        return color;
    }

    public char getConsumoEnergetico() {
        return consumoEnergetico;
    }

    public double getPeso() {
        return peso;
    }


}
