package ejercicio4;

public class Television extends Electrodomestico implements Precio {

    private final static int  RESOLUCION_DEF = 20;
    private final static boolean TDT_DEF = false;

    private int resolucion;
    private boolean sintonizadorTDT;


    public Television() {
        this(PRECIO_DEF,COLOR_DEF,CONSUMO_DEF,PESO_DEF,RESOLUCION_DEF,TDT_DEF);
    }

    public Television(double precioBase, double peso) {
        this(precioBase,COLOR_DEF,CONSUMO_DEF,peso,RESOLUCION_DEF,TDT_DEF);
    }

    public Television(double precioBase, String color, char consumoEnergetico, double peso, int resolucion, boolean sintonizadorTDT) {
        super(precioBase, color, consumoEnergetico, peso);
        this.resolucion = resolucion;
        this.sintonizadorTDT = sintonizadorTDT;
    }

    public int getResolucion() {
        return resolucion;
    }

    public boolean isSintonizadorTDT() {
        return sintonizadorTDT;
    }

    @Override
    public double precioFinal() {
        double precio = super.precioFinal();

        if (resolucion > 40) {
            precio += precioBase*0.3;
        }

        if (sintonizadorTDT) {
            precio += 50;
        }

        return precio;
    }
}
