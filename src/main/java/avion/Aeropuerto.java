package avion;

import java.util.Calendar;

/*
* Clase aeropuestro
* @autor albert sanchez
* */

public abstract class Aeropuerto {

    private static int idAutomatico = 1;
    private final int MAX_AVIONES = 20;

    private String nombre;
    private int id;
    private Direccion direccion;
    private int anoInaguracion;
    private int capacidad;
    private Avion[] aviones;
    private int numeroAviones;

    public Aeropuerto() {
        this.id = idAutomatico++;
        aviones= new Avion[MAX_AVIONES];
        this.numeroAviones = 0;

    }

    public Aeropuerto(String nombre, String pais, String calle, int numero, String ciudad, int anoInaguracion, int capacidad) {
        this.id = idAutomatico++;
        this.nombre = nombre;
        this.direccion = new Direccion(pais,calle,numero,ciudad);
        this.anoInaguracion = anoInaguracion;
        this.capacidad = capacidad;
        aviones= new Avion[MAX_AVIONES];
        this.numeroAviones = 0;
    }


    public Aeropuerto(String nombre, Direccion direccion, int anoInaguracion, int capacidad) {
        this.id = idAutomatico++;
        this.nombre = nombre;
        this.direccion = direccion;
        this.anoInaguracion = anoInaguracion;
        this.capacidad = capacidad;
        aviones= new Avion[MAX_AVIONES];
        this.numeroAviones = 0;

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }



    public int getAnoInaguracion() {
        return anoInaguracion;
    }

    public void setAnoInaguracion(int anoInaguracion) {
        this.anoInaguracion = anoInaguracion;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }


    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public int getNumeroAviones() {
        return numeroAviones;
    }

    public int  aniosAbierto(){
      int anioActual = Calendar.getInstance().get(Calendar.YEAR);
      return anioActual - this.anoInaguracion;
    }




    public void anadirAvion( Avion a){

        if (numeroAviones < MAX_AVIONES) {
            aviones[numeroAviones] = a;
            numeroAviones++;
        }else {
            System.out.println("No se pueden meter mas aviones");
        }
    }

    public abstract void ganaciasTotales(double cantidadGanada);

    @Override
    public String toString() {
        return "avion.Aeropuerto{"+ "con id: "+ this.id +
                " nombre='" + nombre + '\'' +
                ", direccion=" + direccion +
                ", anoInaguracion=" + anoInaguracion +
                ", capacidad=" + capacidad +
                '}'+ "cuenta con los siguientes aviones: \n" + mostrarAviones();
    }

    private String mostrarAviones(){
        String avionesStr = "";
        for (int i = 0; i < numeroAviones; i++) {

            if (this.aviones[i].isActivado()) {
                avionesStr += this.aviones[i].toString() + "\n";
            }
        }
        return avionesStr;
    }
}
