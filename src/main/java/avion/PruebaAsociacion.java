package avion;

public class PruebaAsociacion {

    public static void main(String[] args) {

        Direccion d = new Direccion("España","mentiras",1,"ciudad rea");

        Aeropuerto a = new AereopuertoPrivado("Ciudad Real",d,2000,10000,5);

        Avion av = new Avion("Boing 347",200,400.0);

        Avion av2 = new Avion("Boing 477",200,200.0);

        a.anadirAvion(av);
        a.anadirAvion(av2);

        av.setActivado(true);
        av2.setActivado(true);

        System.out.println(a.getNumeroAviones());

        System.out.println(a);

    }
}
