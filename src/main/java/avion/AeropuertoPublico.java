package avion;

public class AeropuertoPublico extends Aeropuerto {

    private double financiacion;
    private int numTrabajadoreDiscapacitados;


    public AeropuertoPublico(double financiacion, int numTrabajadoreDiscapacitados) {
        super();
        this.financiacion = financiacion;
        this.numTrabajadoreDiscapacitados = numTrabajadoreDiscapacitados;
    }

    public AeropuertoPublico(String nombre, String pais, String calle, int numero, String ciudad, int anoInaguracion, int capacidad, double financiacion, int numTrabajadoreDiscapacitados) {
        super(nombre, pais, calle, numero, ciudad, anoInaguracion, capacidad);
        this.financiacion = financiacion;
        this.numTrabajadoreDiscapacitados = numTrabajadoreDiscapacitados;
    }

    public AeropuertoPublico(String nombre, Direccion direccion, int anoInaguracion, int capacidad, double financiacion, int numTrabajadoreDiscapacitados) {
        super(nombre, direccion, anoInaguracion, capacidad);
        this.financiacion = financiacion;
        this.numTrabajadoreDiscapacitados = numTrabajadoreDiscapacitados;
    }

    public double getFinanciacion() {
        return financiacion;
    }

    public int getNumTrabajadoreDiscapacitados() {
        return numTrabajadoreDiscapacitados;
    }

    @Override
    public void ganaciasTotales(double cantidadGanada) {
        double ganancia = cantidadGanada + this.financiacion + (this.numTrabajadoreDiscapacitados * 100);
        System.out.println("La cantidad ganada a sido de " + ganancia);
    }

    @Override
    public String toString() {
        return super.toString() + " El publico y la financiacion por parte del estado es de " + financiacion + " y hay" + numTrabajadoreDiscapacitados+ " trabajadores discapacitados";
    }
}
