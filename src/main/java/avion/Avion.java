package avion;

public class Avion implements Activable {

    // Atributos
    private String modelo;
    private int nAsientos;
    private double velocidadMaxima;
    private boolean activado;

    // Contructores
    public Avion (){
        this.modelo = "";
        this.nAsientos = 0;
        this.velocidadMaxima  = 0;

    }

    public Avion (String modelo, int nAsientos, Double velocidadMaxima){
        this.modelo = modelo;
        this.nAsientos = nAsientos;
        this.velocidadMaxima = velocidadMaxima;
        this.activado = false;
    }


    public String getModelo(){
        return modelo;
    }

    public int getnAsientos() {
        return nAsientos;
    }

    public double getVelocidadMaxima() {
        return velocidadMaxima;
    }


    public void setModelo(String modelo){
        this.modelo = modelo;
    }


    public void setnAsientos(int nAsientos) {
        this.nAsientos = nAsientos;
    }

    public void setVelocidadMaxima(double velocidadMaxima) {
        this.velocidadMaxima = velocidadMaxima;
    }

    @Override
    public String toString() {
        return "avion.Avion{" +
                "modelo='" + modelo + '\'' +
                ", nAsientos=" + nAsientos +
                ", velocidadMaxima=" + velocidadMaxima +
                '}';
    }

    @Override
    public boolean isActivado() {
        return activado;
    }

    @Override
    public void setActivado(boolean value) {
        this.activado = value;
    }
}
