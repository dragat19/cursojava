package avion;

public interface Activable {

    boolean isActivado();
    void setActivado(boolean value);
}
