package avion;

public class AereopuertoPrivado extends Aeropuerto {
    private int numSocios;

    public AereopuertoPrivado(int numSocios) {
        super();
        this.numSocios = numSocios;
    }

    public AereopuertoPrivado(String nombre, String pais, String calle, int numero, String ciudad, int anoInaguracion, int capacidad, int numSocios) {
        super(nombre, pais, calle, numero, ciudad, anoInaguracion, capacidad);
        this.numSocios = numSocios;
    }

    public AereopuertoPrivado(String nombre, Direccion direccion, int anoInaguracion, int capacidad, int numSocios) {
        super(nombre, direccion, anoInaguracion, capacidad);
        this.numSocios = numSocios;
    }

    public int getNumSocios() {
        return numSocios;
    }

    public void setNumSocios(int numSocios) {
        this.numSocios = numSocios;
    }

    @Override
    public void ganaciasTotales(double cantidadGanada) {

        double ganancia = cantidadGanada/ this.numSocios;

        System.out.println("La cantidad ganada por cada socio es de " + ganancia);

    }

    @Override
    public String  toString() {
        return super.toString() + " Es privado y tiene " + numSocios + " socios";
    }
}
