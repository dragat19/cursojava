package ejercicio5;

public class PrubaEjercicio5 {
    public static void main(String[] args) {

        Serie listaSeries[] = new Serie[5];
        Videojuego listaVideojuegos[] = new Videojuego[5];

        //Creamos un objeto en cada posicion del array
        listaSeries[0]=new Serie();
        listaSeries[1]=new Serie("Juego de tronos", "George R. R. Martin ");
        listaSeries[2]=new Serie("Los Simpsons", 25, "Humor", "Matt Groening");
        listaSeries[3]=new Serie("Padre de familia", 12 ,"Humor", "Seth MacFarlane");
        listaSeries[4]=new Serie("Breaking Bad", 5, "Thriller", "Vince Gilligan");

        listaVideojuegos[0]=new Videojuego();
        listaVideojuegos[1]=new Videojuego("Assasin creed 2", 30, "Aventura", "EA");
        listaVideojuegos[2]=new Videojuego("God of war 3", "Santa Monica");
        listaVideojuegos[3]=new Videojuego("Super Mario 3DS", 30, "Plataforma", "Nintendo");
        listaVideojuegos[4]=new Videojuego("Final fantasy X", 200, "Rol", "Square Enix");



        listaSeries[3].entregar();
        listaSeries[0].entregar();
        listaVideojuegos[2].entregar();
        listaVideojuegos[4].entregar();

        int numSeriesEntrgadas  = 0;
        int numVideoJuegosEntrgados  = 0;


        for (int i = 0; i < listaSeries.length; i++) {
            if (listaSeries[i].isEntregado()){
                numSeriesEntrgadas += 1;
                listaSeries[i].devolver();
            }

            if (listaVideojuegos[i].isEntregado()){
                numVideoJuegosEntrgados += 1;
                listaVideojuegos[i].devolver();
            }
        }


        System.out.println("Hay "+numSeriesEntrgadas+" Series entregados");
        System.out.println("Hay "+numVideoJuegosEntrgados+" Videojuegos entregados");

        Serie serieMayor = listaSeries[0];
        Videojuego videoMayor = listaVideojuegos[0];

        for (int i = 1; i < listaSeries.length ; i++) {
            if (listaSeries[i].compareTo(serieMayor) == Serie.MAYOR){
                serieMayor = listaSeries[i];
            }

            if (listaVideojuegos[i].compareTo(videoMayor) == Videojuego.MAYOR){
                videoMayor = listaVideojuegos[i];
            }
        }


        System.out.println(videoMayor);
        System.out.println(serieMayor);

    }
}
