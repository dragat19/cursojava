package ejercicio5;

public class Serie implements  Entregable{

    private final static int TEMPORADA_DEF = 3;
    private final static boolean ENTREGADO_DEF = false;

    // Estados para el metodo compareTo
    public   final static  int MENOR = -1;
    public   final static  int MAYOR = 1;
    public   final static  int IGUAL = 0;

    private String titulo;
    private int numTemporada;
    private boolean entregado;
    private String genero;
    private String creador;


    public Serie() {
        this("",TEMPORADA_DEF,"","");
    }

    public Serie(String titulo, String creador) {
        this(titulo,TEMPORADA_DEF,"",creador);
    }

    public Serie(String titulo, int temporada, String genero, String creador) {
        this.titulo = titulo;
        this.numTemporada = temporada;
        this.genero = genero;
        this.creador = creador;
    }


    public String getTitulo() {
        return titulo;
    }

    public int getNumTemporada() {
        return numTemporada;
    }

    public String getGenero() {
        return genero;
    }

    public String getCreador() {
        return creador;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setNumTemporada(int numTemporada) {
        this.numTemporada = numTemporada;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }





    @Override
    public String toString() {
        return "Serie{" +
                "titulo='" + titulo + '\'' +
                ", numTemporada=" + numTemporada +
                ", entregado=" + entregado +
                ", genero='" + genero + '\'' +
                ", creador='" + creador + '\'' +
                '}';
    }

    @Override
    public void entregar() {
        entregado = true;
    }

    @Override
    public void devolver() {
        entregado = false;
    }

    @Override
    public boolean isEntregado() {
        return entregado;
    }

    @Override
    public int compareTo(Object a) {
        int estado = MENOR;

        Serie ref = (Serie) a;

        if (numTemporada > ref.numTemporada) {
            estado = MAYOR;
        }else {
            if (numTemporada == ref.numTemporada){
                estado = IGUAL;
            }
        }
        return estado;
    }
}
