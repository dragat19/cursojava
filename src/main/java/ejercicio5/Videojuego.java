package ejercicio5;

public class Videojuego implements Entregable{

    private final static int HORA_ESTIMADA_DEF = 10;
    private final static boolean ENTREGADO_DEF = false;

    public   final static  int MENOR = -1;
    public   final static  int MAYOR = 1;
    public   final static  int IGUAL = 0;

    private String titulo;
    private int horaEstimada;
    private boolean entregado;
    private String genero;
    private String compania;

    public Videojuego() {
        this("",HORA_ESTIMADA_DEF,"","");
    }

    public Videojuego(String titulo, String compania) {
        this(titulo,HORA_ESTIMADA_DEF,"",compania);
    }

    public Videojuego(String titulo, int horaEstimada, String genero, String compania) {
        this.titulo = titulo;
        this.horaEstimada = horaEstimada;
        this.genero = genero;
        this.compania = compania;
    }


    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getHoraEstimada() {
        return horaEstimada;
    }

    public void setHoraEstimada(int horaEstimada) {
        this.horaEstimada = horaEstimada;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getCompania() {
        return compania;
    }

    public void setCompania(String compania) {
        this.compania = compania;
    }






    @Override
    public String toString() {
        return "Videojuego{" +
                "titulo='" + titulo + '\'' +
                ", horaEstimada=" + horaEstimada +
                ", entregado=" + entregado +
                ", genero='" + genero + '\'' +
                ", compania='" + compania + '\'' +
                '}';
    }

    @Override
    public void entregar() {
        entregado = true;
    }

    @Override
    public void devolver() {
        entregado = false;
    }

    @Override
    public boolean isEntregado() {
        return entregado;
    }

    @Override
    public int compareTo(Object a) {
        int estado = MENOR;

        Videojuego ref = (Videojuego) a;

        if (horaEstimada > ref.horaEstimada) {
            estado = MAYOR;
        }else {
            if (horaEstimada == ref.horaEstimada){
                estado = IGUAL;
            }
        }
        return estado;
    }
}
