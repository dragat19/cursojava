package ejercicio7;

public class Raices {

    private double a;
    private double b;
    private double c;

    Raices(double a, double b, double c){
        this.a = a;
        this.b = b;
        this.c = c;
    }

    private void obtenerRaices(){
//        (-b±√((b^2)-(4*a*c)))/(2*a)
        double x1 = (-b + Math.sqrt( ((b*b) - (4*a*c))) / (2*a) );
        double x2 = (-b - Math.sqrt( ((b*b) - (4*a*c))) / (2*a) );

        System.out.println("Solucion 1: " +  x1);
        System.out.println("Solucion 2: " + x2);
    }

    private void  obtenerRaiz() {
        double x1 = -b / (2*a);
        System.out.println("Solucion " + x1);
    }

    private  double getDescriminante(){
        double delta = (b*b) - (4*a*c);
        return delta;
    }

    private boolean tieneRacices(){
        return  getDescriminante() > 0 ;
    }


    private  boolean tieneRaiz(){
        return  getDescriminante() == 0;
    }


    public void calcular(){
        if (tieneRacices()){
            obtenerRaices();
        }else {
            if (tieneRaiz()){
                obtenerRaiz();
            }else {
                System.out.println("No tiene posibles soluciones");
            }

        }
    }



}
