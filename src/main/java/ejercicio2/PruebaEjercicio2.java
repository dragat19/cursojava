package ejercicio2;

import javax.swing.*;
import java.util.Locale;
import java.util.Scanner;



/*
Ahora, crea una clase ejecutable que haga lo siguiente:
        Pide por teclado el nombre, la edad, sexo, peso y altura.

        Crea 3 objetos de la clase anterior, el primer objeto obtendrá las anteriores variables pedidas por teclado,
        el segundo objeto obtendrá todos los anteriores menos el peso y la altura y el último por defecto,
        para este último utiliza los métodos set para darle a los atributos un valor.

        Para cada objeto, deberá comprobar si esta en su peso ideal, tiene sobrepeso o por debajo de su peso ideal con un mensaje.

        Indicar para cada objeto si es mayor de edad.

        Por último, mostrar la información de cada objeto.

        Puedes usar métodos en la clase ejecutable, para que os sea mas fácil.
*/

public class PruebaEjercicio2 {


    public static void main(String[] args) {

        // Introducimos los datos


        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\n");
        sc.useLocale(Locale.US);

        //Introducimos los datos
        System.out.println("Introduce el nombre");
        String nombre = sc.next();

        System.out.println("Introduce la edad");
        int edad = sc.nextInt();

        System.out.println("Introduce el sexo");
        char sexo = sc.next().charAt(0);

        System.out.println("Introduce el peso");
        double peso = sc.nextDouble();

        System.out.println("Introduce la altura");
        double altura = sc.nextDouble();

       /* String nombre = JOptionPane.showInputDialog("Introducir Nombre");

        String texto = JOptionPane.showInputDialog("Introducir la edad");
        int edad = Integer.parseInt(texto);

        texto= JOptionPane.showInputDialog("Introducir el sexo");
        char sexo = texto.charAt(0);

        texto = JOptionPane.showInputDialog("Introducir el peso");
        double peso = Double.parseDouble(texto);

        texto = JOptionPane.showInputDialog("Introducir la altura");
        double altura = Double.parseDouble(texto);*/

        // Crear objetos
        Persona persona1 = new Persona();
        Persona persona2 = new Persona(nombre,edad,sexo);
        Persona persona3 = new Persona(nombre,edad,sexo,peso,altura);

        persona1.setNombre("Rosa");
        persona1.setEdad(28);
        persona1.setSexo('F');
        persona1.setAltura(1.57);
        persona1.setPeso(68);

        persona2.setPeso(72);
        persona2.setAltura(1.69);

        System.out.println("Persona 1");
        muestaMsjPeso(persona1);
        muestraMsjMayorEdad(persona1);
        System.out.println(persona1.toString());

        System.out.println("Persona 2");
        muestaMsjPeso(persona2);
        muestraMsjMayorEdad(persona2);
        System.out.println(persona2.toString());


        System.out.println("Persona 3");
        muestaMsjPeso(persona3);
        muestraMsjMayorEdad(persona3);
        System.out.println(persona3.toString());

    }


    public static void muestaMsjPeso(Persona p){
        switch (p.calcularIMC()){
            case Persona.INFRAPESO:
                System.out.println("Persona esta por debajo de su peso ideal");
                break;
            case Persona.PESO_IDEAL:
                System.out.println("Persona esta en su peso ideal");
                break;
            case Persona.SOBREPESO:
                System.out.println("Persona esta por encima de su peso ideal");
                break;
        }
    }


    public static void muestraMsjMayorEdad(Persona p){
        if (p.esMayorDeEdad()){
            System.out.println("La persona es mayor de edad");
        }else {
            System.out.println("La persona no es mayor de edad");
        }
    }


}
