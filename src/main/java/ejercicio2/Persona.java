package ejercicio2;


import java.util.Random;

public class Persona {

    private final static char SEXO_DEF = 'M';

    public final static int INFRAPESO = -1;

    public final static int PESO_IDEAL = 0;

    public final static int SOBREPESO = 1;


    private String nombre;
    private int edad;
    private String dni;
    private char sexo;
    private double peso;
    private double altura;


    public Persona() {
        this("", 0, SEXO_DEF, 0, 0);
    }


    public Persona(String nombre, int edad, char sexo) {
        this(nombre, edad, sexo, 0, 0);
    }


    public Persona(String nombre, int edad, char sexo, double peso, double altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        comprobarSexo();
        this.peso = peso;
        this.altura = altura;
        generaDNI();
    }


    /* calcularIMC(): calculara si la persona esta en su peso ideal (peso en kg/(altura^2  en m)),
    si esta fórmula devuelve un valor menor que 20, la función devuelve un -1,
    si devuelve un número entre 20 y 25 (incluidos), significa que esta por debajo de su peso ideal la función devuelve un 0
    y si devuelve un valor mayor que 25 significa que tiene sobrepeso, la función devuelve un 1.
    Te recomiendo que uses constantes para devolver estos valores.*
    */

    public int calcularIMC() {
        double pesoActual = this.peso / Math.pow(altura, 2);
        if (pesoActual >= 20 && pesoActual <= 25) {
            return PESO_IDEAL;
        } else {
            if (pesoActual < 20) {
                return INFRAPESO;
            } else {
                return SOBREPESO;
            }
        }
    }

    //esMayorDeEdad(): Indica si es mayor de edad, devuelve un booleano.

    public boolean esMayorDeEdad(){
        boolean mayorDeEdad = false;
        if (edad > 18) mayorDeEdad = true;
        return  mayorDeEdad;
    }

    //comprobarSexo(char sexo): Comprueba que el sexo introducido es correcto. Si no es correcto, sera H. No sera visible al exterior.
    public void comprobarSexo(){
        if (this.sexo != 'M' && this.sexo != 'H'){
            this.sexo = SEXO_DEF;
        }
    }


    /* generaDNI(): genera un número aleatorio de 8 cifras, genera a partir de este su número su letra correspondiente.
    Este método sera invocado cuando se construya el objeto. Puedes dividir el método para que te sea más fácil.
    No será visible al exterior.*/

    public void generaDNI(){
        int divisor = 23;
        Random random = new Random();
        int dni8 = random.nextInt(90000000)+10000000;
        int restoDni = dni8 % divisor;
        char letraDni = generarLetraDni(restoDni);
        this.dni = Integer.toString(dni8) + "-" + letraDni;
    }

    public char generarLetraDni(int restoDni){
        char[] tablaLetraDni = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};
        return tablaLetraDni[restoDni];
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    @Override
    public String toString() {
        return "Informacion de la persona: \n" +
                "Nombre: " + nombre + '\n' +
                "Edad: " + edad + " años \n"+
                "DNI: " + dni + '\n' +
                "Sexo: " + sexo + "\n" +
                "Peso: " + peso + " Kg \n" +
                "Altura: " + altura + " Metros \n";
    }
}
